//
//  AppDelegate.h
//  RobLog
//
//  Created by Stephen Lakowske on 2/7/14.
//  Copyright (c) 2014 Cal Poly Hackathon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
